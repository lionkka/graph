import express from 'express'
import cors from 'cors'
import request from 'request'

const smallFileURL = 'https://ccfilesforestry.blob.core.windows.net/demo/sample_stands_10000.csv'

const app = express()

app.use(cors())

app.get('/file', function (req, res) {
  request.get(smallFileURL)
  request(smallFileURL).pipe(res);
})

app.listen(3001, () => {
  console.log('Server is running on 3001 port')
})
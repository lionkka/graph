import React, { Component } from 'react'
import HistorgamsContainer from './components/HistorgamsContainer'
import MapContainer from './components/MapContainer'
import Scatterplot from './components/ScatterplotComponent'
import Papa from 'papaparse'
import {
  AppBar,
  Tab,
  Tabs,
  FormControl,
  withStyles,
  MenuItem,
  Select,
  CircularProgress,
  InputLabel
} from '@material-ui/core'

const mainSpeciesOptions = ['any', 'pine', 'spruce', 'birch', 'other']
const datasets = [
  'volPerHaCache',
  'ageYearsCache',
  'sizeInHaCache',
  'scatterplotAgeYearsCache',
  'mapCache'
]

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: 5,
    minWidth: 120,
    maxWidth: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
})

class App extends Component {
  state = {
    data: null,
    filterBy: mainSpeciesOptions[0],
    tabToShow: 0,
    // insert default objects
    ...datasets.reduce((reducer, item) => {
      reducer[item] = {}
      return reducer
    }, {}),
  }

  async componentDidMount () {

    // get file from server
    Papa.parse('http://127.0.0.1:3001/file', {
      download: true,
      header: true,
      complete: ({data}, error, c) => {
        if (error) {
          console.error(error)
        }
        // data = data.slice(0, 1000)
        const dataToUpdate = {}

        //set default dataset for each type
        datasets.forEach((field) => dataToUpdate[field] = {any: this.calculateParams[field](data)})
        this.setState({
          data,
          ...dataToUpdate
        })
      }
    })
  }

  calculateHistogram = (data, y, x, step, filter) => {
    const buckets = {}
    data.forEach((item) => {
      if (filter && filter !== item[y]) {
        return null
      }
      const key = !step ? item[x] : Math.floor(item[x] / step) * step
      const value = buckets[key] || 0
      buckets[key] = value + 1
    })
    return Object.entries(buckets).map(([x, y]) => ({x, y}))
  }

  calculateScatterplot = (data, y, x, filterBy, fieldToFilter) => {
    let result = data

    if (filterBy) {
      result = data.filter((item) => filterBy !== item[fieldToFilter])
    }
    result = result.map((item) => {
      return [+item[x], +item[y]]
    })
    return result

  }

  calculateParams = {
    volPerHaCache: (data, filterBy) => this.calculateHistogram(data, 'main_species', 'vol_m3_per_ha', 10, filterBy),
    ageYearsCache: (data, filterBy) => this.calculateHistogram(data, 'main_species', 'age_years', 10, filterBy),
    sizeInHaCache: (data, filterBy) => this.calculateHistogram(data, 'main_species', 'size_in_ha', null, filterBy),
    scatterplotAgeYearsCache: (data, filterBy) => this.calculateScatterplot(data, 'age_years', 'size_in_ha', filterBy, 'main_species'),
    mapCache: (data, filterBy) => !filterBy ? data : data.filter(({main_species}) => main_species === filterBy),
  }

  onChangeSpecies = ({target: {value: filterBy}}) => {
    const dataToUpdate = {}
    datasets
      .forEach((field) => {
        const obj = this.state[field]
        if (!obj.hasOwnProperty(filterBy)) {
          const cache = {...obj}
          cache[filterBy] = this.calculateParams[field](this.state.data, filterBy)
          dataToUpdate[field] = cache
        }
      })
    this.setState({filterBy, ...dataToUpdate})
  }

  onChangeTab = (e, tabToShow) => {
    this.setState({tabToShow})
  }

  render () {
    const {
      filterBy,
      mapCache,
      scatterplotAgeYearsCache,
      tabToShow
    } = this.state
    const {classes} = this.props
    if (!this.state.data) return <CircularProgress className="progress"/>

    return (
      <div className="App">
        <div className="container">
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="species">Choose species</InputLabel>
            <Select
              value={filterBy}
              onChange={this.onChangeSpecies}
              inputProps={{id: 'species',}}
            >
              {mainSpeciesOptions.map((option) => <MenuItem key={option} value={option}>{option}</MenuItem>)}
            </Select>
          </FormControl>
          <div className="tabs">
            <AppBar position="static">
              <Tabs fullWidth centered value={tabToShow} onChange={this.onChangeTab}>
                <Tab label="Histogram"/>
                <Tab label="Map"/>
                <Tab label="Scatterplot"/>
              </Tabs>
            </AppBar>
          </div>
        </div>
        <div>
          {tabToShow === 0 && <HistorgamsContainer {...{data: this.state, filterBy}} />}
          {tabToShow === 1 && <MapContainer data={mapCache[filterBy]}/>}
          {tabToShow === 2 &&
          <Scatterplot
            data={scatterplotAgeYearsCache[filterBy]}
            margin={{bottom: 70, top: 10, left: 50}}
          />}
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(App)

import React, { Component, Fragment } from 'react'
import HistogramComponent from './HistogramComponent'

import {
  AppBar,
  Tab,
  Tabs,
} from '@material-ui/core'

const types = [
  {
    label: 'Wood volume', value: 'volPerHa'
  },
  {
    label: 'Average age of trees', value: 'ageYears'
  },
  {
    label: 'Size of forest stand', value: 'sizeInHa'
  }]

class HistorgamsContainer extends Component {
  state = {
    type: 0
  }

  onChangeType = (e, type) => {
    this.setState({type})
  }

  render () {

    const {type} = this.state
    const {filterBy, data} = this.props
    const dataToShow = types[type].value + 'Cache'
    return (
      <Fragment>
        <div className={'container'}>
          <AppBar position="static" className={'tabs'}>
            <Tabs centered fullWidth value={type} onChange={this.onChangeType}>
              {types.map(({label}) => <Tab key={label} label={label}/>)}
            </Tabs>
          </AppBar>
        </div>
        <HistogramComponent
          xLabel={types[type].label}
          yLabel={'Value'}
          data={data[dataToShow][filterBy]}/>
      </Fragment>
    )
  }
}

export default HistorgamsContainer
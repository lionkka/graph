import React from 'react'
import { Group } from '@vx/group'
import { GlyphCircle } from '@vx/glyph'
import { Line } from '@vx/shape'
import { scaleLinear } from '@vx/scale'
import { max } from 'd3-array'
import { AxisLeft, AxisBottom } from '@vx/axis'

const x = d => d[0]
const y = d => d[1]
const width = 800
const height = 600
const margin = {bottom: 70, top: 10, left: 50}
const lineColor = '#8e205f'
class Scatterplot extends React.Component {
  state = {
    yMax: null,
    yScale: null,
    xScale: null,
  }

  static getDerivedStateFromProps ({data}) {
    if (!data) return {}
    const xMax = width
    const yMax = height - 80

    return {
      yMax,
      yScale: scaleLinear({
        domain: [0, max(data, y)],
        range: [yMax, 0],
      }),
      xScale: scaleLinear({
        domain: [0, max(data, x)],
        range: [0, xMax],
      })
    }
  }

  numTicksForWidth = (width) => {
    if (width <= 300) return 2
    if (300 < width && width <= 400) return 5
    return 10
  }

  render () {
    const {yScale, xScale} = this.state
    const {data} = this.props

    return (
      <div className="scatterplot">
        <svg width={width} height={height}>
          <Group
            top={margin.top}
            left={margin.left}
          >
            {data.map((point, i) => {
              return (
                <GlyphCircle
                  className="dot"
                  key={`point-${point.x}-${i}`}
                  fill={'rgba(23, 233, 217, .5)'}
                  left={xScale(x(point))}
                  top={yScale(y(point))}
                  size={12}
                />
              )
            })}
          </Group>
          <Group
            top={margin.top}
            left={margin.left}
          >
            <Line
              from={{x: 0, y: height - margin.bottom}}
              to={{x: width, y: height - margin.bottom}}
              stroke={lineColor}
            />
            <AxisLeft
              scale={this.state.yScale}
              bottom={margin.bottom}
              hideZero
              numTicks={5}
              label="value"
              labelProps={{
                fill: lineColor,
                textAnchor: 'middle',
                fontSize: 12,
                fontFamily: 'Arial'
              }}
              stroke="#1b1a1e"
              tickStroke="#8e205f"
              tickLabelProps={() => ({
                fill: lineColor,
                textAnchor: 'end',
                fontSize: 10,
                fontFamily: 'Arial',
                dx: '-0.25em',
                dy: '0.25em'
              })}
              tickComponent={({formattedValue, ...tickProps}) => (
                <text {...tickProps}>{formattedValue}</text>
              )}
            />
            <AxisBottom
              top={height - margin.bottom}
              left={0}
              scale={this.state.xScale}
              numTicks={this.numTicksForWidth(width)}
              label="m³"
              labelProps={{
                fill: lineColor,
                textAnchor: 'middle',
                fontSize: 12,
                fontFamily: 'Arial'
              }}
            >{props => {
              const tickLabelSize = 10
              const tickRotate = 90
              const axisCenter = (props.axisToPoint.x - props.axisFromPoint.x) / 2
              return (
                <g className="my-custom-bottom-axis">
                  {props.ticks.map((tick, i) => {
                    const tickX = tick.to.x
                    const tickY = tick.to.y + tickLabelSize + props.tickLength
                    return (
                      <Group key={`vx-tick-${tick.value}-${i}`} className={'vx-axis-tick'}>
                        <Line from={tick.from} to={tick.to} stroke={lineColor}/>
                        <text
                          transform={`translate(${tickX}, ${tickY}) rotate(${tickRotate})`}
                          fontSize={tickLabelSize}
                          textAnchor="middle"
                          fill={'#8e205f'}
                        >
                          {tick.formattedValue}
                        </text>
                      </Group>
                    )
                  })}
                  <text textAnchor="middle" transform={`translate(${axisCenter}, 50)`} fontSize="8">
                    {props.label}
                  </text>
                </g>
              )
            }}
            </AxisBottom>
          </Group>
        </svg>
      </div>
    )
  }

}

export default Scatterplot
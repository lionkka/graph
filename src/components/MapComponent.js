import React from 'react'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow
} from 'react-google-maps'
import marker from '../images/icon.png'

export const MapComponent = withScriptjs(withGoogleMap(({onMarkerClick, isMarkerShown, data, markerId}) =>
  <GoogleMap
    defaultZoom={7}
    defaultCenter={{lat: 61.8299, lng: 22.1362}}
  >
    {isMarkerShown && data.map(item =>
      <Marker
        icon={{url: marker}}
        key={item.standid}
        position={{lat: +item.latitude, lng: +item.longitude}}
        onClick={() => onMarkerClick(item.standid)}
      >
        {markerId === item.standid &&
        <InfoWindow key={item.standid} position={{lat: item.latitude, lng: item.longitude}}>
          <div>
            <p><span>id:</span> {item.standid}</p>
            <p><span>main species:</span> {item.main_species}</p>
            <p><span>vol:</span> {item.vol_m3_per_ha}</p>
            <p><span>age:</span> {item.age_years}</p>
            <p><span>size:</span> {item.size_in_ha}</p>
          </div>
        </InfoWindow>
        }
      </Marker>
    )}
  </GoogleMap>
))
import React, { Component } from 'react'
import { Bar, Line } from '@vx/shape'
import { Group } from '@vx/group'
import { scaleBand, scaleLinear } from '@vx/scale'
import { max } from 'd3-array'
import { AxisLeft, AxisBottom } from '@vx/axis'

const width = 800
const height = 600
const margin = {bottom: 70, top: 50, left: 75, right: 0}

const numTicksForWidth = (width) => {
  if (width <= 300) return 2
  if (300 < width && width <= 400) return 5
  return 10
}

const numTicksForHeight = (height) => {
  if (height <= 300) return 3
  if (300 < height && height <= 600) return 5
  return 10
}

class HistogramComponent extends Component {
  state = {
    data: null,
    yMax: null,
    yScale: null,
    xScale: null,
  }

  static getDerivedStateFromProps ({data}) {
    if (!data) return {}

    const yMax = height - 120
    const xMax = width - margin.left - margin.right
    return {
      yMax,
      xMax,
      xScale: scaleBand({
        rangeRound: [0, xMax],
        domain: data.map(d => d.x),
      }),
      yScale: scaleLinear({
        rangeRound: [yMax, 0],
        domain: [0, max(data, d => d.y)],
      }),
    }
  }

  render () {
    const {xScale, yMax, xMax, yScale} = this.state
    const {xLabel, yLabel, data} = this.props
    if (!data) return null
    return (
      <svg width={width} height={height}>
        <Group
          top={margin.top}
          left={margin.left}
          width={xMax}
          height={yMax}
        >
          {data.map(({x, y}) => {
            const barHeight = yMax - yScale(y)
            return (
              <Group key={`bar-${x}`}>
                <Bar
                  width={xScale.bandwidth()}
                  height={barHeight}
                  x={xScale(x)}
                  y={yMax - barHeight}
                  fill="rgba(23, 233, 217, .5)"
                  fillOpacity="rgba(23, 233, 217)"
                  data={{x, y}}
                  strokeWidth={1}
                  stroke="#8e205f"
                />
              </Group>
            )
          })}
        </Group>
        <Group
          left={margin.left}
        >
          <AxisLeft
            top={margin.top}
            left={0}
            scale={yScale}
            hideZero
            numTicks={numTicksForHeight(height)}
            label={yLabel}
            labelProps={{
              fill: '#8e205f',
              textAnchor: 'middle',
              fontSize: 12,
              fontFamily: 'Arial'
            }}
            stroke="#1b1a1e"
            tickStroke="#8e205f"
            tickLabelProps={() => ({
              fill: '#8e205f',
              textAnchor: 'end',
              fontSize: 10,
              fontFamily: 'Arial',
              dx: '-0.25em',
              dy: '0.25em'
            })}
            tickComponent={({formattedValue, ...tickProps}) => (
              <text {...tickProps}>{formattedValue}</text>
            )}
          />
          <AxisBottom
            top={height - margin.bottom}
            left={0}
            scale={xScale}
            numTicks={numTicksForWidth(width)}
            label={xLabel}
          >{props => {
            const tickLabelSize = 10
            const tickRotate = 90
            const tickColor = '#8e205f'
            const axisCenter = (props.axisToPoint.x - props.axisFromPoint.x) / 2
            return (
              <g className="my-custom-bottom-axis">
                {props.ticks.map((tick, i) => {
                  const tickX = tick.to.x
                  const tickY = tick.to.y + tickLabelSize + props.tickLength
                  return (
                    <Group key={`vx-tick-${tick.value}-${i}`} className={'vx-axis-tick'}>
                      <Line from={tick.from} to={tick.to} stroke={tickColor}/>
                      <text
                        transform={`translate(${tickX}, ${tickY}) rotate(${tickRotate})`}
                        fontSize={tickLabelSize}
                        textAnchor="middle"
                        fill={'#8e205f'}
                      >
                        {tick.formattedValue}
                      </text>
                    </Group>
                  )
                })}
                <text textAnchor="middle" transform={`translate(${axisCenter}, 50)`} fontSize="8">
                  {props.label}
                </text>
              </g>
            )
          }}
          </AxisBottom>
        </Group>
      </svg>
    )
  }
}

export default HistogramComponent
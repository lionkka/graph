import React, { Component } from 'react'
import { CircularProgress } from '@material-ui/core'
import { MapComponent } from './MapComponent'

class MapContainer extends Component {
  state = {
    markerId: null
  }

  onMarkerClick = item => {
    this.setState({markerId: item})
  }

  render () {
    return (
      <div className={`mapHolder`}>
        <MapComponent
          isMarkerShown
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2eYjBRIAo_RTSuVpawonLxoIv1TDvHDI&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<CircularProgress className="progress"/>}
          containerElement={<div style={{height: `100%`}}/>}
          mapElement={<div style={{height: `100%`}}/>}
          data={this.props.data}
          onMarkerClick={this.onMarkerClick}
          markerId={this.state.markerId}
        />
      </div>
    )
  }
}

export default MapContainer